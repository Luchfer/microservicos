package br.com.mastertech.cartao.client.exception;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteCustomErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        switch (response.reason()){
            case "Cliente nao encontrado":
                return new ClienteNotFoundException();
            default:
                return new Exception("Generic error");
        }
    }


}
