package br.com.mastertech.cartao.model.dto.request;

import javax.validation.constraints.NotNull;

public class CartaoByIdRequest {
    @NotNull
    private long id;

    public CartaoByIdRequest(){}

    public CartaoByIdRequest(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
