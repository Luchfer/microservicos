package br.com.mastertech.cartao.repository;


import br.com.mastertech.cartao.model.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartaoRepository extends JpaRepository<Cartao, Long> {
    Optional<Cartao> getByNumero(String numero);
    Optional<Cartao> getByIdAndAtivo(Long numero,Boolean ativo);
    Optional<Cartao> getByIdAndIdCliente(Long id,Long idCliente);
}
