package br.com.mastertech.cartao.client.exception;

import br.com.mastertech.cartao.client.Cliente;
import br.com.mastertech.cartao.client.dto.ClienteDTO;
import com.netflix.client.ClientException;

public class ClienteLoadBalanceFallBack implements Cliente {


    private Exception exception;

    public ClienteLoadBalanceFallBack(Exception exception) {
        this.exception = exception;
    }


    @Override
    public ClienteDTO getClienteById(long id) {
        if(exception.getCause() instanceof ClientException){
            throw new ClienteUnavailableException();
        }
        throw (RuntimeException) exception;
    }
}
