package br.com.mastertech.cartao.client;

import br.com.mastertech.cartao.client.exception.ClienteCustomErrorDecoder;
import br.com.mastertech.cartao.client.exception.ClienteFallBack;
import br.com.mastertech.cartao.client.exception.ClienteLoadBalanceFallBack;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


public class ClienteConfiguration {

    @Bean
    public ErrorDecoder errorDecoder() {
        return new ClienteCustomErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClienteFallBack(), RetryableException.class)
                .withFallbackFactory(ClienteLoadBalanceFallBack::new,RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
