package br.com.mastertech.cartao.client;

import br.com.mastertech.cartao.client.dto.ClienteDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cliente", configuration = ClienteConfiguration.class)
public interface Cliente {
    @GetMapping("/cliente/{id}")
    ClienteDTO getClienteById(@PathVariable long id);

}
