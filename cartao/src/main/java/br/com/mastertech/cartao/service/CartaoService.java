package br.com.mastertech.cartao.service;


import br.com.mastertech.cartao.client.Cliente;
import br.com.mastertech.cartao.client.exception.ClienteNotFoundException;
import br.com.mastertech.cartao.client.dto.ClienteDTO;
import br.com.mastertech.cartao.exception.CartaoNotFoundException;
import br.com.mastertech.cartao.exception.CartaoNotFoundOrInactiveException;
import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.repository.CartaoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private Cliente clienteService;

    public Cartao create(Cartao cartao) {
        ClienteDTO byId = null;
        cartao.setAtivo(false);
        try {
            byId = clienteService.getClienteById(cartao.getIdCliente());
        } catch (FeignException.FeignClientException.NotFound e){
            throw new ClienteNotFoundException();
        }

        return cartaoRepository.save(cartao);
    }

    public Cartao changeAtivo(Long idCartao,String numero, Boolean ativo) {
        Cartao cartao = new Cartao();
        if(idCartao == null){
            cartao = getByNumero(numero);
        }else{
            cartao = getById(idCartao);
        }
        cartao.setAtivo(ativo);

        return cartaoRepository.save(cartao);
    }

    public Cartao getById(Long id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.getByIdAndAtivo(id,true);

        if(!cartaoOptional.isPresent()) {
            throw new CartaoNotFoundOrInactiveException();
        }

        return cartaoOptional.get();
    }

    public Cartao getByIdClienteAndIdCartao(Long id,Long idCliente) {
        Optional<Cartao> cartaoOptional = cartaoRepository.getByIdAndIdCliente(id,idCliente);

        if(!cartaoOptional.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return cartaoOptional.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> cartaoOptional = cartaoRepository.getByNumero(numero);

        if(!cartaoOptional.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return cartaoOptional.get();
    }
}

