package br.com.mastertech.cartao.model.dto.request;


import com.sun.istack.NotNull;

public class CartaoChangeAtivoRequest {

    @NotNull
    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
