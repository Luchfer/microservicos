package br.com.mastertech.cliente;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.WeightedResponseTimeRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {
    @Bean
    public IRule ribbonRule() {
        return new WeightedResponseTimeRule();
    }
}
