package br.com.mastertech.cliente.service;


import br.com.mastertech.cliente.exception.ClienteNotFoundException;
import br.com.mastertech.cliente.model.Cliente;
import br.com.mastertech.cliente.model.ClienteMapper;
import br.com.mastertech.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private ClienteMapper clienteMapper;

    public Cliente createCliente(Cliente dadosCliente){
        dadosCliente = clienteRepository.save(dadosCliente);
        return dadosCliente;
    }

    public Cliente getCliente(Long idCliente){

        Optional<Cliente> clienteOptional = clienteRepository.findById(idCliente);

        if(!clienteOptional.isPresent()) {
            throw new ClienteNotFoundException();
        }

        return clienteOptional.get();


    }


}
