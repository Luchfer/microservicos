package br.com.mastertech.fatura.controller;

import br.com.mastertech.fatura.model.FaturaMapper;
import br.com.mastertech.fatura.response.ExpiraResponse;
import br.com.mastertech.fatura.response.FaturaPagaResponse;
import br.com.mastertech.fatura.response.FaturaResponse;
import br.com.mastertech.fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @Autowired
    private FaturaMapper faturaMapper;

    @GetMapping("/{idCliente}/{idCartao}")
    public List<FaturaResponse> getFatura(@PathVariable long idCliente,@PathVariable long idCartao){

        return faturaMapper.toResponseList(faturaService.getPagamentos(idCliente,idCartao));
    }

    @GetMapping("/{idCliente}/{idCartao}/pagar")
    public FaturaPagaResponse pagar(@PathVariable long idCliente, @PathVariable long idCartao){

        return faturaMapper.toFaturaPagaResponse(faturaService.pagar(idCliente,idCartao));
    }
    @PostMapping("/{idCliente}/{idCartao}/expirar")
    public ExpiraResponse expirar(@PathVariable long idCliente, @PathVariable long idCartao){
        if(faturaService.bloqueiaCartao(idCliente,idCartao)){
            return new ExpiraResponse("OK");
        }
        return new ExpiraResponse("NOK");
    }
}
