package br.com.mastertech.fatura.clients;

import br.com.mastertech.fatura.clients.dto.PagamentoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name="pagamento")
public interface PagamentoClient {

    @GetMapping("pagamentos/{idCartao}")
    List<PagamentoDTO> getPagamentosByCartao (@PathVariable long idCartao);

    @GetMapping("pagamentos/{idCartao}/fatura")
    List<PagamentoDTO> getPagamentosByCartaoAndData(@PathVariable long idCartao);

    @DeleteMapping("pagamentos/{idCartao}")
    Long deletePagamentosByCartao(@PathVariable long idCartao);
}
