package br.com.mastertech.fatura.clients.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Pagamento não encontado")
public class PagamentoNotFoundException extends RuntimeException{
}
