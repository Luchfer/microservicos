package br.com.mastertech.fatura.clients;

import br.com.mastertech.fatura.clients.dto.CartaoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name="cartao",configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("cartao/{idCartao}/cliente/{idCliente}")
    CartaoDTO getCartaoByClientAndId(@PathVariable long idCliente, @PathVariable long idCartao);

    @PostMapping("cartao/{idCartao}/expirar")
    CartaoDTO bloqueiaCartao(@PathVariable long idCartao);
}
