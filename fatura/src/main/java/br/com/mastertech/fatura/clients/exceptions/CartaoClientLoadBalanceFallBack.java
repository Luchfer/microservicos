package br.com.mastertech.fatura.clients.exceptions;


import br.com.mastertech.fatura.clients.CartaoClient;
import br.com.mastertech.fatura.clients.dto.CartaoDTO;
import com.netflix.client.ClientException;

public class CartaoClientLoadBalanceFallBack implements CartaoClient {


    private Exception exception;

    public CartaoClientLoadBalanceFallBack(Exception exception) {
        this.exception = exception;
    }


    @Override
    public CartaoDTO getCartaoByClientAndId(long idCliente, long idCartao) {
        if(exception.getCause() instanceof ClientException){
            throw new CartaoNotAvailable();
        }
        throw (RuntimeException) exception;
    }

    @Override
    public CartaoDTO bloqueiaCartao(long idCartao) {
        if(exception.getCause() instanceof ClientException){
            throw new CartaoNotAvailable();
        }
        throw (RuntimeException) exception;
    }
}
