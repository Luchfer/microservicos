package br.com.mastertech.fatura.request;

public class BloqueiaCartaoRequest {

    public Boolean ativo;

    public BloqueiaCartaoRequest(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
