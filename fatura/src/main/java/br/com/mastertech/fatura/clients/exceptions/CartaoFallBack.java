package br.com.mastertech.fatura.clients.exceptions;


import br.com.mastertech.fatura.clients.CartaoClient;
import br.com.mastertech.fatura.clients.dto.CartaoDTO;

public class CartaoFallBack implements CartaoClient {
    @Override
    public CartaoDTO getCartaoByClientAndId(long idCliente, long idCartao) {
        throw new CartaoNotAvailable();
    }

    @Override
    public CartaoDTO bloqueiaCartao(long idCartao) {
        throw new CartaoNotAvailable();
    }
}
