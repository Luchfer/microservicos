package br.com.mastertech.fatura.model;

import br.com.mastertech.fatura.clients.dto.PagamentoDTO;
import br.com.mastertech.fatura.response.FaturaPagaResponse;
import br.com.mastertech.fatura.response.FaturaResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FaturaMapper {


    public List<FaturaResponse> toResponseList(List<PagamentoDTO> pagamentos){
        List<FaturaResponse> response = new ArrayList<FaturaResponse>();
        for (PagamentoDTO pagamento:pagamentos) {
            FaturaResponse responseData = new FaturaResponse();
            responseData.setId(pagamento.getId());
            responseData.setDescricao(pagamento.getDescricao());
            responseData.setCartao_id(pagamento.getCartao_id());
            responseData.setValor(pagamento.getValor());
            response.add(responseData);
        }

        return response;
    }

    public FaturaPagaResponse toFaturaPagaResponse(Fatura dadosFatura){
        FaturaPagaResponse faturaPagaResponse = new FaturaPagaResponse();
        faturaPagaResponse.setId(dadosFatura.getId());
        faturaPagaResponse.setDataPagamento(dadosFatura.getDataPagamento());
        faturaPagaResponse.setValorPago(dadosFatura.getValorPago());
        return faturaPagaResponse;
    }
}
