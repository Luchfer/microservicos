package br.com.mastertech.fatura.clients.exceptions;


import br.com.mastertech.fatura.clients.PagamentoClient;
import br.com.mastertech.fatura.clients.dto.PagamentoDTO;

import java.util.List;

public class PagamentoFallBack implements PagamentoClient {

    @Override
    public List<PagamentoDTO> getPagamentosByCartao(long idCartao) {
        throw new PagamentoNotAvailableException();
    }

    @Override
    public List<PagamentoDTO> getPagamentosByCartaoAndData(long idCartao) {
        throw new PagamentoNotAvailableException();
    }

    @Override
    public Long deletePagamentosByCartao(long idCartao) {
        throw new PagamentoNotAvailableException();
    }
}
