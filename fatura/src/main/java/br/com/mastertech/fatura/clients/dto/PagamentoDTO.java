package br.com.mastertech.fatura.clients.dto;

import java.math.BigDecimal;

public class PagamentoDTO {
    private long id;
    private long cartao_id;
    private String descricao;
    private BigDecimal valor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(long cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
