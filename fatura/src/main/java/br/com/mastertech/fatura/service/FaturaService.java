package br.com.mastertech.fatura.service;

import br.com.mastertech.fatura.clients.CartaoClient;
import br.com.mastertech.fatura.clients.PagamentoClient;
import br.com.mastertech.fatura.clients.dto.CartaoDTO;
import br.com.mastertech.fatura.clients.dto.PagamentoDTO;
import br.com.mastertech.fatura.clients.exceptions.CartaoNotFoundException;
import br.com.mastertech.fatura.clients.exceptions.PagamentoNotFoundException;
import br.com.mastertech.fatura.model.Fatura;
import br.com.mastertech.fatura.repository.FaturaRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class FaturaService {

    @Autowired
    private CartaoClient cartaoClient;

    @Autowired
    private PagamentoClient pagamentoClient;

    @Autowired
    FaturaRepository faturaRepository;

    public  List<PagamentoDTO>  getPagamentos(long idCliente,long idCartao){
        List<PagamentoDTO> list = new ArrayList<PagamentoDTO>();
       if(validaClienteCartao(idCliente,idCartao)){
           try{
               list = pagamentoClient.getPagamentosByCartao(idCartao);
           }catch(FeignException.FeignClientException.NotFound e){
               throw new PagamentoNotFoundException();
           }
       }
       return list;
    }

    public Fatura pagar(long idCliente, long idCartao){
        if(validaClienteCartao(idCliente,idCartao)){
            List<PagamentoDTO> list = new ArrayList<PagamentoDTO>();
            try{
                list = pagamentoClient.getPagamentosByCartaoAndData(idCartao);
            }catch(FeignException.FeignClientException.NotFound e){
                throw new PagamentoNotFoundException();
            }
            Fatura dadosFatura = efetuaPagamento(list,idCartao,idCliente);
            long pagamentos = 0;
            if(dadosFatura != null){
                pagamentos = pagamentoClient.deletePagamentosByCartao(idCartao);
            }
            return dadosFatura;
        }
        return null;
    }

    private Fatura efetuaPagamento(List<PagamentoDTO>  pagamentos,long idCartao,long idCliente){
        BigDecimal valorFatura= pagamentos.stream().map(pagamentoDTO -> pagamentoDTO.getValor()).reduce(BigDecimal.ZERO, BigDecimal::add);
        Fatura dadosFatura = new Fatura();
        dadosFatura.setIdCartao(idCartao);
        dadosFatura.setIdCliente(idCliente);
        dadosFatura.setValorPago(valorFatura);
        dadosFatura.setDataPagamento(LocalDateTime.now());
        return faturaRepository.save(dadosFatura);
    }

    public Boolean validaClienteCartao(long idCliente, long idCartao){
        CartaoDTO dadosCartao = null;
        try{
            dadosCartao = cartaoClient.getCartaoByClientAndId(idCliente,idCartao);
        }catch(Exception e){
           System.out.println(e.getClass());
        }
        return true;
    }

    public Boolean bloqueiaCartao(long idCliente, long idCartao){
        if(validaClienteCartao(idCliente,idCartao)){
            CartaoDTO dadosCartao = null;
            try{
                dadosCartao = cartaoClient.bloqueiaCartao(idCartao);
            }catch(FeignException.FeignClientException.NotFound e){
                throw new CartaoNotFoundException();
            }
            return true;
        }
        return false;
    }


}
