package br.com.mastertech.fatura.response;

public class ExpiraResponse {


    public String status;

    public ExpiraResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
