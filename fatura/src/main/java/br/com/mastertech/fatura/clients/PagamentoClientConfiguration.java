package br.com.mastertech.fatura.clients;


import br.com.mastertech.fatura.clients.exceptions.PagamentoFallBack;
import feign.Feign;
import feign.RetryableException;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;


public class PagamentoClientConfiguration {

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new PagamentoFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
