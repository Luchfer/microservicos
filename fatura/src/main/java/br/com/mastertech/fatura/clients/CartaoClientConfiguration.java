package br.com.mastertech.fatura.clients;


import br.com.mastertech.fatura.clients.exceptions.CartaoClientLoadBalanceFallBack;
import br.com.mastertech.fatura.clients.exceptions.CartaoCustomErrorDecoder;
import br.com.mastertech.fatura.clients.exceptions.CartaoFallBack;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;


public class CartaoClientConfiguration {

    @Bean
    public ErrorDecoder errorDecoder() {
        return new CartaoCustomErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CartaoFallBack(), RetryableException.class)
                .withFallbackFactory(CartaoClientLoadBalanceFallBack::new,RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
