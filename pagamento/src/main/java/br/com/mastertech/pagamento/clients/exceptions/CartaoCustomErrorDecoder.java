package br.com.mastertech.pagamento.clients.exceptions;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoCustomErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        switch (response.reason()){
            case "Cartao nao encontrado ou inativo":
                return new CartaoNotActiveException();
            case "Cartao nao encontrado":
                return new CartaoNotFoundException();
            default:
                return new Exception("Generic error");
        }
    }


}
