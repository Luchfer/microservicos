package br.com.mastertech.pagamento.repository;

import br.com.mastertech.pagamento.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento, Long> {

    List<Pagamento> findByCartaoId(long idCartao);

    List<Pagamento> findByCartaoIdAndDataCompraLessThanEqual(long idCartao, LocalDateTime dataPagamento);

    Long deleteByCartaoId(long idCartao);

}
