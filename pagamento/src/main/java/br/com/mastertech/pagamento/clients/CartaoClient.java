package br.com.mastertech.pagamento.clients;

import br.com.mastertech.pagamento.clients.dto.CartaoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cartao", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    CartaoDTO getCartaoById(@PathVariable long id);



}
