package br.com.mastertech.pagamento.model;

import br.com.mastertech.pagamento.request.CreatePagamentoRequest;
import br.com.mastertech.pagamento.response.CreatePagamentoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    public CreatePagamentoResponse pagamentoToResponse(Pagamento pagamento){
        CreatePagamentoResponse response = new CreatePagamentoResponse();
        response.setId(pagamento.getId());
        response.setCartao_id(pagamento.getCartaoId());
        response.setDescricao(pagamento.getDescricao());
        response.setValor(pagamento.getValor());
        return response;
    }

    public Pagamento toPagamento(CreatePagamentoRequest request){
        Pagamento pagamento = new Pagamento();
        pagamento.setCartaoId(request.getCartao_id());
        pagamento.setDescricao(request.getDescricao());
        pagamento.setValor(request.getValor());
        return pagamento;
    }

    public List<CreatePagamentoResponse> toListReponse(List<Pagamento> pagamentos){
        List<CreatePagamentoResponse> listaReponse = new ArrayList<>();
        pagamentos.forEach(pagamento->{
            listaReponse.add(new CreatePagamentoResponse(pagamento.getId(),pagamento.getCartaoId(),pagamento.getDescricao(),pagamento.getValor()));
        });
        return listaReponse;
    }

}
