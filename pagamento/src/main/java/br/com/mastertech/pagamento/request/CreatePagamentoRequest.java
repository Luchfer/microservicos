package br.com.mastertech.pagamento.request;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CreatePagamentoRequest {

    @NotNull
    private long cartao_id;
    @NotNull
    private String descricao;
    @NotNull
    @DecimalMin(value = "0.01", inclusive = false)
    @Digits(integer=3, fraction=2)
    private BigDecimal valor;

    public long getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(long cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
