package br.com.mastertech.pagamento.clients.exceptions;

import br.com.mastertech.pagamento.clients.CartaoClient;
import br.com.mastertech.pagamento.clients.dto.CartaoDTO;

public class CartaoFallBack implements CartaoClient {
    @Override
    public CartaoDTO getCartaoById(long id) {
       throw new CartaoNotAvailable();
    }
}
