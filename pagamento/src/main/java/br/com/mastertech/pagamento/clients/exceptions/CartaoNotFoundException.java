package br.com.mastertech.pagamento.clients.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cartao não foi encontrado :c")
public class CartaoNotFoundException extends RuntimeException{

}
