package br.com.mastertech.pagamento.service;

import br.com.mastertech.pagamento.clients.CartaoClient;
import br.com.mastertech.pagamento.clients.dto.CartaoDTO;
import br.com.mastertech.pagamento.clients.exceptions.CartaoNotActiveException;
import br.com.mastertech.pagamento.clients.exceptions.CartaoNotFoundException;
import br.com.mastertech.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.repository.PagamentoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento create(Pagamento dadosPagamento){
        CartaoDTO cartaoDTO= null;
        try{
            cartaoDTO = cartaoClient.getCartaoById(dadosPagamento.getCartaoId());
        }catch(FeignException.FeignClientException.NotFound e){
            throw new CartaoNotFoundException();
        }
        if(!cartaoDTO.isAtivo()){
            throw new CartaoNotActiveException();
        }
        dadosPagamento.setDataCompra(LocalDateTime.now());
        return pagamentoRepository.save(dadosPagamento);
    }

    public List<Pagamento> getPagamentosPorCartao(long idCartao){
        List<Pagamento> list = new ArrayList<>();
        CartaoDTO dadosCartao = null;
        try{
            dadosCartao = cartaoClient.getCartaoById(idCartao);
        }catch(FeignException.FeignClientException.NotFound e){
            throw new CartaoNotFoundException();
        }

        return pagamentoRepository.findByCartaoId(dadosCartao.getId());
    }

    public List<Pagamento> getPagamentosPorCartaoEData(long idCartao){
        List<Pagamento> list = new ArrayList<>();
        CartaoDTO dadosCartao = null;
        try{
            dadosCartao = cartaoClient.getCartaoById(idCartao);
        }catch(FeignException.FeignClientException.NotFound e){
            throw new CartaoNotFoundException();
        }

        return pagamentoRepository.findByCartaoIdAndDataCompraLessThanEqual(dadosCartao.getId(),LocalDateTime.now());
    }

    public Long detetePagamentosByCartao(long idCartao){
        return pagamentoRepository.deleteByCartaoId(idCartao);
    }
}
