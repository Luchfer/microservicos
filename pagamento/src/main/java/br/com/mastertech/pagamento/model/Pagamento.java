package br.com.mastertech.pagamento.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Component
@Table(name = "pagamento")
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long cartaoId;
    private String descricao;
    private BigDecimal valor;
    private LocalDateTime dataCompra;


    public Pagamento(){}

    public Pagamento(long id, long cartaoId, String descricao, BigDecimal valor) {
        this.id = id;
        this.cartaoId = cartaoId;
        this.descricao = descricao;
        this.valor = valor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public LocalDateTime getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(LocalDateTime dataCompra) {
        this.dataCompra = dataCompra;
    }

}
