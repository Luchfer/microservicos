package br.com.mastertech.pagamento.clients.exceptions;

import br.com.mastertech.pagamento.clients.CartaoClient;
import br.com.mastertech.pagamento.clients.dto.CartaoDTO;
import com.netflix.client.ClientException;

public class CartaoClientLoadBalanceFallBack implements CartaoClient {


    private Exception exception;

    public CartaoClientLoadBalanceFallBack(Exception exception) {
        this.exception = exception;
    }


    @Override
    public CartaoDTO getCartaoById(long id) {
        if(exception.getCause() instanceof ClientException){
            throw new CartaoNotAvailable();
        }
        throw (RuntimeException) exception;
    }
}
