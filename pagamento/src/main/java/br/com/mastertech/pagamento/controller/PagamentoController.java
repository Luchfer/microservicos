package br.com.mastertech.pagamento.controller;

import br.com.mastertech.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.model.PagamentoMapper;
import br.com.mastertech.pagamento.request.CreatePagamentoRequest;
import br.com.mastertech.pagamento.response.CreatePagamentoResponse;
import br.com.mastertech.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;
    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public CreatePagamentoResponse createPagamento(@Valid @RequestBody CreatePagamentoRequest request){

        Pagamento pagamento = pagamentoMapper.toPagamento(request);
        pagamento = pagamentoService.create(pagamento);
        return pagamentoMapper.pagamentoToResponse(pagamento);
    }

    @GetMapping("pagamentos/{idCartao}")
    public List<CreatePagamentoResponse> getExtrato(@PathVariable long idCartao){
        List<Pagamento> pagamentos = pagamentoService.getPagamentosPorCartao(idCartao);
        return pagamentoMapper.toListReponse(pagamentos);

    }
    @GetMapping("pagamentos/{idCartao}/fatura")
    public List<CreatePagamentoResponse> getExtratoAtualPorCartao(@PathVariable long idCartao){
        List<Pagamento> pagamentos = pagamentoService.getPagamentosPorCartaoEData(idCartao);
        return pagamentoMapper.toListReponse(pagamentos);

    }
    @DeleteMapping("pagamentos/{idCartao}")
    public ResponseEntity<String> deletePagamentosByCartao(@PathVariable long idCartao){
        Long deletados = pagamentoService.detetePagamentosByCartao(idCartao);
        return ResponseEntity.status(HttpStatus.OK).body("Pagamentos apagados");
    }

}
